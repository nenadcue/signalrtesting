﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using System.Diagnostics;
using SignalRDemo_001.Models;
using System.Threading;

namespace SignalRDemo_001
{
    public class MyHub : Hub
    {
        bool groupSelected = false;
        public void Hello()
        {
            Clients.All.hello();
        }

        public void BroadcastServerTime()
        {
            Clients.All.MessageReceiver(DateTime.Now);
        }

        public void GetConnectionID()
        {
            //Clients.All.SendConnectionID(this.Context.ConnectionId);
            string resultMsg = $"Your ID is: " + this.Context.ConnectionId;
            Clients.Client(this.Context.ConnectionId).SendConnectionID(resultMsg);
            //Clients.All.SendConnectionID(resultMsg);
        }

        public async Task Handshake(string group)
        {
            await Groups.Add(Context.ConnectionId, group);
        }
        
        public override Task OnDisconnected(bool ljkahsd)
        {
            Debug.WriteLine("diskonektovao se klijent");
            return base.OnDisconnected(ljkahsd);
        }

        public override Task OnConnected()
        {
            Debug.WriteLine(">>>>> new client connected!");
            return base.OnConnected();
        }

        public void SendMessagePropertyChange(PropertyChange businessObject, string group)
        {

            Debug.WriteLine("Message received from client: " + Context.ConnectionId);

            PropertyChange change = businessObject;
            PropertyChange serverChangedObject = new PropertyChange();
            serverChangedObject.ObjectName = change.ObjectName + "-added by server";
            serverChangedObject.PropertyName = change.PropertyName + "-added by server";
            Clients.Group(group).SendMessage(serverChangedObject);
        }

    }
}