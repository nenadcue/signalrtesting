﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace SignalRDemo_001.Models
{
    public class PropertyChange
    {
        public PropertyChange()
        {

        }

        public PropertyChange(string rawJson)
        {
            try
            {
                PropertyChange tmp = JsonConvert.DeserializeObject<PropertyChange>(rawJson);
                ObjectName = tmp.ObjectName;
                PropertyName = tmp.PropertyName;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(">>>>>>> " + ex.Message);
            }
        }

        [JsonProperty("objectName")]
        public string ObjectName { get; set; }

        [JsonProperty("propertyName")]
        public string PropertyName { get; set; }
    }
}